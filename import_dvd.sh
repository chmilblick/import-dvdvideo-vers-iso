#!/bin/bash

# Import de DVD vers une ISO
# Créé le 2022-07-03

# Par défaut, lecteur 0
lecteur="/dev/sr0"

# Par défaut, ne pas faire le Hanshake avec VLC
unlock=0
test=0

UnlockDVD()
{
	echo "Handshake VLC sur $lecteur"
	cvlc --run-time 6 --start-time 16 $lecteur vlc://quit 
}

Help()
{
   # Display Help
   echo "Importer un DVD vidéo en ISO"
   echo
   echo "options:"
   echo "h     Aide"
   echo "l     Lister les lecteurs DVD"
   echo "s     Choix d'un numéro de lecteur sr (par défaut sr0)"
   echo "u     Réaliser un Handshake avec VLC pour faire sauter certaines protections"
   echo "t     Tester : tout lancer sauf l'importation avec dd"
   echo
}

ListDVD()
{
	#Lister les lecteurs DVD
	echo "Liste des lecteurs DVD"
	lsblk | grep -E " rom | TYPE "
}

# https://www.redhat.com/sysadmin/arguments-options-bash-scripts
while getopts "s:ulhto:" option; do
	case $option in
		h) # display Help
			Help
			exit;;
		l) # list dvd reader
			ListDVD
			exit;;
		s) # Choose a reader
			lecteur="/dev/sr$OPTARG"
			echo "Lecteur ciblé $lecteur";;
		u) # unlock
	      		unlock=1;;
	      	t) # Test
	      		test=1;;
	      	o) # Nom de sortie
	      		nom_fichier_iso="$OPTARG.iso"
	      		echo "Nom de fichier cible $nom_fichier_iso";;
		\?) # Invalid option
			echo "Error: Invalid option"
			exit;;
	esac
done


if [ $unlock -eq 1 ]; then
	UnlockDVD
fi

# Récupération des informations
#TODO : trouver un moyen de vérifer que $lecteur soit correctement assigné
isoinfo_rez=$(isoinfo -d -i $lecteur)

nb_blocs=$(echo $isoinfo_rez | grep -Po '(?<=Volume size is: )\d+')
nom=$(echo $isoinfo_rez | grep -Po '(?<=Volume id: )\w+')
taille_bloc=$(echo $isoinfo_rez | grep -Po '(?<=Logical block size is: )\d+')

# Si le nom de fichier de sortie n'est pas définie, alors prendre
# celui retrouvé avec isoinfo

if [ -z "$nom_fichier_iso" ];
	then
		nom_fichier_iso="$nom.iso"
fi

# Vérifier si le fichier existe déjà. Si cela est le cas pour l'instant provoquer une erreur.
# -e test pour tout type de fichier (fichier, dossier, volume...)
if [ -e "$nom_fichier_iso" ];
	then
		echo "$nom_fichier_iso existe déjà. Veuillez renommer."
		exit
fi

taille_octets=$(($nb_blocs*$taille_bloc))

# Afficher proprement en Go la taille ce qui permettra d'avoir une idée de l'avancée
taille_Go=$(echo "go=$taille_octets / 10^9; scale=2; print go/1,\"\n\"" | bc -l)
echo "Import de $nom ($taille_octets octets, $taille_Go Go) depuis $lecteur vers $nom_fichier_iso"

date
# Import
if [ $test -ne 1 ]; then
	dd if=$lecteur of=$nom_fichier_iso bs=$taille_bloc count=$nb_blocs status=progress conv=notrunc,noerror,sync
fi

echo "Fin d'import"
date

eject $lecteur
