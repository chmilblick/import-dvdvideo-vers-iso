# Import DVD vidéo iso

Script Bash pour importer des DVD Vidéo en Bash.

## Pour l'utiliser

Pour importer un dvd depuis le lecteur /dev/sr1 avec un handshake VLC

`sh import_dvd.sh -u -s 1`

Pour l'aide, l'option -h

## Information

Ce script est livré tel quel et a été développé pour mon usage personnel. n'hésitez pas à me faire part de vos remarques.

